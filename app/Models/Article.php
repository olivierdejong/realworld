<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Article extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var string[]
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var string[]
     */
    protected $hidden = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'slug',
        'title',
        'description',
        'body',
        'favorited_count',
    ];

    protected static function boot(): void
    {
        parent::boot();

        static::saving(function ($article): void {
            $count = Article::where('title', $article->title)->count();
            $slugExt = $count || '';
            $article->slug = Str::slug($article->title) . $slugExt;

            // TODO: reduce queries
            while (Article::where('slug', $article->slug)->count()) {
                $slugExt += 1;
                $article->slug = Str::slug($article->title) . $slugExt;
            }
        });
    }

    public function delete(): bool
    {
        $this->comments()->delete();
        return parent::delete();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(Profile::class, 'profile_id');
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    public function followers(): BelongsToMany
    {
        return $this->belongsToMany(Profile::class, 'favorites', 'article_id', 'profile_id')->withTimestamps();
    }
}
