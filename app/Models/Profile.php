<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Profile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'username',
        'bio',
        'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var string[]
     */
    protected $hidden = [
        'id', 'created_at', 'updated_at',
    ];

    public function follow(Profile $profile): void
    {
        $this->leaders()->attach($profile->id);
    }

    public function unfollow(Profile $profile): void
    {
        $this->leaders()->detach($profile->id);
    }

    public function isFollowing(int $profileId): bool
    {
        return $this->leaders->contains($profileId);
    }

    public function favorite(Article $article): Article
    {
        $this->favorites()->attach($article->id);
        return $article;
    }

    public function unfavorite(Article $article): Article
    {
        $this->favorites()->detach($article->id);
        return $article;
    }

    public function hasFavorited(int $articleId): bool
    {
        return $this->favorites->contains($articleId);
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'username';
    }

    public function articles(): HasMany
    {
        return $this->hasMany(Article::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function followers(): BelongsToMany
    {
        return $this->belongsToMany(Profile::class, 'followers', 'leader_id', 'follower_id')->withTimestamps();
    }

    public function leaders(): BelongsToMany
    {
        return $this->belongsToMany(Profile::class, 'followers', 'follower_id', 'leader_id')->withTimestamps();
    }

    public function favorites(): BelongsToMany
    {
        return $this->belongsToMany(Article::class, 'favorites', 'profile_id', 'article_id')->withTimestamps();
    }
}
