<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CreateCommentRequest;
use App\Http\Resources\CommentCollection;
use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use App\Http\Resources\Comment as CommentResource;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function get(Article $article): JsonResponse
    {
        $commentsQuery = $article->comments()
            ->orderBy('created_at', 'desc');

        return (new CommentCollection($commentsQuery->get()))->response();
    }

    public function create(CreateCommentRequest $request, Article $article): JsonResponse
    {
        $validated = $request->validated();

        $comment = new Comment($validated);
        $comment->author()->associate(auth()->user()->profile);
        $comment->article()->associate($article);
        $comment->save();

        return (new CommentResource($comment))->response();
    }

    public function delete(Request $request, Article $article, Comment $comment): JsonResponse
    {
        if (!$article->comments->contains($comment->id)) {
            return response()->json(['body' => ['Something Went Wrong']], 403);
        }

        if (!$comment->delete()) {
            return response()->json(['body' => ['Comment could not be Deleted']], 403);
        };

        return response()->json(['body' => ['Comment deleted Successfully']], 200);
    }
}
