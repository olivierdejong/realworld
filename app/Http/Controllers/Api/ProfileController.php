<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Profile as ProfileResource;
use Illuminate\Http\JsonResponse;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function get(Profile $profile): JsonResponse
    {
        return (new ProfileResource($profile))->response();
    }

    public function follow(Profile $profile): JsonResponse
    {
        $authProfile = auth()->user()->profile;

        if ($authProfile->isFollowing($profile->id)) {
            return response()->json(['error' => 'Already Following Profile'], 403);
        }

        $authProfile->follow($profile);
        $authProfile->refresh();

        return (new ProfileResource($profile))->response();
    }

    public function unfollow(Profile $profile): JsonResponse
    {
        $authProfile = auth()->user()->profile;

        if (!$authProfile->isFollowing($profile->id)) {
            return response()->json(['error' => 'Not Following Profile'], 403);
        }

        $authProfile->unfollow($profile);
        $authProfile->refresh();

        return (new ProfileResource($profile))->response();
    }
}
