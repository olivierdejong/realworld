<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Tag as TagResource;
use App\Models\Tag;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function get(): JsonResponse
    {
        return (TagResource::collection(Tag::all()))->response();
    }

    public function count(Request $request): object
    {
        $retrieved = $request->query('limit');
        $limit = is_null($retrieved) ? 10 : $retrieved;
        return Tag::withCount('articles')->orderBy('articles_count', 'DESC')->limit($limit)->get();
    }
}
