<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CreateArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\Article as ArticleResource;
use App\Models\Article;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    public function list(Request $request): JsonResponse
    {
        $articlesQuery = Article::query()
            ->orderBy('created_at', 'desc');

        $tag = $request->query('tag');
        if (!is_null($tag)) {
            $articlesQuery->whereHas('tags', function (Builder $query) use ($tag): void {
                $query->where('name', $tag);
            });
        }

        $author = $request->query('author');
        if (!is_null($author)) {
            $articlesQuery->whereHas('author', function (Builder $query) use ($author): void {
                $query->where('username', $author);
            });
        }

        $favorited = $request->query('favorited');
        if (!is_null($favorited)) {
            $articlesQuery->whereHas('followers', function (Builder $query) use ($favorited): void {
                $query->where('username', $favorited);
            });
        }

        $articlesCollection = new ArticleCollection($this->runScopedQuery($articlesQuery, $request->query('limit'), $request->query('offset')));
        return $articlesCollection->response();
    }

    public function feed(Request $request): JsonResponse
    {
        $profile = auth()->user()->profile;
        $idArray = $profile->leaders->pluck('id');

        $articlesQuery = Article::query()
            ->whereIn('profile_id', $idArray)
            ->orderBy('created_at', 'desc');

        $articlesCollection = new ArticleCollection($this->runScopedQuery($articlesQuery, $request->query('limit'), $request->query('offset')));
        return $articlesCollection->response();
    }

    public function get(Article $article): JsonResponse
    {
        return (new ArticleResource($article))->response();
    }

    public function create(CreateArticleRequest $request): JsonResponse
    {
        $validated = $request->validated();
        $article = auth()->user()->profile->articles()->save(Article::make($validated));

        if (isset($validated['tagList'])) {
            $this->updateTags($validated['tagList'], $article);
        }

        return (new ArticleResource($article))->response();
    }

    public function update(UpdateArticleRequest $request, Article $article): JsonResponse
    {
        $validated = $request->validated();

        if (isset($validated['title'])) {
            $validated['slug'] = Str::slug($validated['title']);
        }

        if (isset($validated['tagList'])) {
            $this->updateTags($validated['tagList'], $article);
        }

        $article->update($validated);

        return (new ArticleResource($article))->response();
    }

    /**
     * Update Taglist
     *
     * @param  string[] $tagList
     * @param  Article $article
     * @return void
     */
    private function updateTags(array $tagList, Article $article): void
    {
        $newTags = array_map(function ($tag) {
            return Tag::firstOrCreate([
                'name' => strtolower($tag)
            ])->id;
        }, array_filter($tagList));

        $article->tags()->sync($newTags);
    }

    public function delete(Article $article): JsonResponse
    {
        if (!$article->delete()) {
            return response()->json('Article Not Deleted', 404);
        }

        return response()->json('Article Deleted Successfully', 200);
    }

    public function favorite(Article $article): JsonResponse
    {
        $profile = auth()->user()->profile;

        if ($profile->hasFavorited($article->id)) {
            return response()->json(['error' => 'Already Favorited Article'], 403);
        }

        $profile->favorite($article);
        $profile->refresh(['favorites']);

        return (new ArticleResource($article))->response();
    }

    public function unfavorite(Article $article): JsonResponse
    {
        $profile = auth()->user()->profile;

        if (!$profile->hasFavorited($article->id)) {
            return response()->json(['error' => 'Article has not been Favorited'], 403);
        }

        $profile->unfavorite($article);
        $profile->refresh(['favorites']);

        return (new ArticleResource($article))->response();
    }

    private function runScopedQuery(Builder $articles, ?int $limit, ?int $offset): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        $limit = is_null($limit) ? 2 : $limit;
        $offset = is_null($offset) ? 0 : $offset;
        $page =  floor($offset / $limit) + 1;

        return $articles
            ->paginate($limit, ['*'], 'page', $page);
    }
}
