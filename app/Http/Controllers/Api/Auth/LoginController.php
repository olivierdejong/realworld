<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\JsonResponse;

class LoginController extends Controller
{
    /**
     * Get a JWT via given credentials.
     *
     * @param  LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $validated = $request->validated();
        $token = auth()->attempt($validated);

        if (!$token) {
            return response()->json(['errors' => ['combination' => 'The combination is incorrect']], 401);
        }

        $user = auth()->user();
        $user->token = $token;

        return (new UserResource($user))->response();
    }

    /**
     * Logout authenticated user
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        $name = auth()->user()->profile->username;
        auth()->logout();

        return response()->json(['message' => $name . ' has been logged out'], 200);
    }
}
