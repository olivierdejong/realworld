<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\User as UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class RegisterController extends Controller
{
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  RegisterRequest $request
     * @return JsonResponse
     */
    protected function store(RegisterRequest $request): JsonResponse
    {
        $validated = $request->validated();

        $user = User::create($validated);
        $user->profile()->create($validated);

        $token = auth()->login($user);
        $user->token = $token;

        return (new UserResource($user))->response();
    }
}
