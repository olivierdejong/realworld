<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\UpdateUserAndProfileRequest;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Get the authenticated User.
     * @param Request $request
     * @return JsonResponse
     */
    public function me(Request $request): JsonResponse
    {
        $user = auth()->user();
        $user->token = $request->bearerToken();

        return (new UserResource($user))->response();
    }

    /**
     * Edit the authenticated User.
     * @param  UpdateUserAndProfileRequest $request
     * @return JsonResponse
     */
    public function update(UpdateUserAndProfileRequest $request): JsonResponse
    {
        $user = auth()->user();
        $credentials = $request->validated();

        $user->update($credentials);
        $user->profile->update($credentials);
        $user->token = $request->bearerToken();

        return (new UserResource($user))->response();
    }
}
