<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserAndProfileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return string[]
     */
    public function rules(): array
    {
        $id = auth()->id();

        if (!$id) {
            return [];
        }

        return [
            'username' => ['string', 'max:255', 'unique:profiles,username,' . $id],
            'email' => ['string', 'email', 'max:255', 'unique:users,email,' . $id],
            'password' => ['string', 'min:8'],
            'bio' => ['nullable', 'string', 'max:255'],
            'image' => ['nullable', 'string'],
        ];
    }
}
