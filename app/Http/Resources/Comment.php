<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     * @inheritDoc
     */
    public function toArray($request)
    {
        self::wrap('comment');

        return [
            'id' => $this->id,
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'body' => $this->body,
            'author' => new Author($this->author),
        ];
    }
}
