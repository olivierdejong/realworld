<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Author extends JsonResource
{
    /** @var bool|null */
    private $following = null;

    /**
     * Transform the resource into an array.
     * @inheritDoc
     */
    public function toArray($request)
    {
        $user = auth()->user();
        if (!is_null($user)) {
            $this->following = $user->profile->isFollowing($this->id);
        }

        return [
            'id' => $this->id,
            'username' => $this->username,
            'bio' => $this->bio,
            'image' => $this->image,
            'following' => $this->when(!is_null($this->following), $this->following),
        ];
    }
}
