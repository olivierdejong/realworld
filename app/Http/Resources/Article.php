<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Author as AuthorResource;
use App\Http\Resources\Tag as TagResource;

class Article extends JsonResource
{
    /** @var bool|null */
    private $favorited = null;

    /**
     * Transform the resource into an array.
     * @inheritDoc
     */
    public function toArray($request)
    {
        self::wrap('article');

        $user = auth()->user();

        if (!is_null($user)) {
            $this->favorited = $user->profile->hasFavorited($this->id);
        }

        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'title' => $this->title,
            'description' => $this->description,
            'body' => $this->body,
            'tagList' => $this->tags->map(function ($tag) {
                return new TagResource($tag);
            }),
            'createdAt' => $this->created_at,
            'updatedAt' => $this->updated_at,
            'favorited' => $this->when(!is_null($this->favorited), $this->favorited),
            'favoritesCount' => count($this->followers),
            'author' => new AuthorResource($this->author),
        ];
    }
}
