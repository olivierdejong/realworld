<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     * @inheritDoc
     */
    public function toArray($request)
    {
        self::wrap('user');

        return [
            'id' => $this->id,
            'email' => $this->email,
            'username' => $this->profile->username,
            'bio' => $this->profile->bio,
            'image' => $this->profile->image,
            'token' => $this->when(!is_null($this->token), $this->token),
        ];
    }
}
