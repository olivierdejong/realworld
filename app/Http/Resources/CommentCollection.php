<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CommentCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     * @inheritDoc
     */
    public function toArray($request)
    {
        self::wrap(null);

        return [
            'comments' => $this->collection,
        ];
    }
}
