<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Tag extends JsonResource
{
    /**
     * Transform the resource into an array.
     * @inheritDoc
     */
    public function toArray($request)
    {
        self::wrap('tags');

        return [
            'id' => $this->id,
            'name' => $this->name,
            ];
    }
}
