<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\UserNotDefinedException;

class CheckJwt
{
    /**
     * Handle an incoming request.
     *
     * @param Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            auth()->userOrFail();
        } catch (UserNotDefinedException $e) {
            return response()->json(['error' => 'Blocked Route: Unauthorized'], 401);
        }

        return $next($request);
    }
}
