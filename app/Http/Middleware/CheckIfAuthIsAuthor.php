<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;

class CheckIfAuthIsAuthor
{
    /**
     * Check if the authenticated user is the author of the requested Object (
     *
     * @param  Request  $request
     * @param  Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // Only return parameters encapsulated by '{ }'
        $routeParameters = array_filter(explode("/", $request->route()->uri()), function ($string) {
            return $string[0] === "{" && $string[-1] === "}";
        });

        $modelName = substr(end($routeParameters), 1, -1);

        try {
            $model = app("App\Models\\$modelName");
        } catch (BindingResolutionException $e) {
            return response()->json(['body' => ['No Model Name Found In Route']], 404);
        }

        $routeKeyName = $model->getRouteKeyName();
        $identifier = $request->route()->originalParameters()[$modelName];

        $instance = $model->where($routeKeyName, $identifier)->first();

        if ($instance->author->id !== auth()->user()->profile->id) {
            return response()->json(['body' => ['Object is Not Yours to Modify']], 401);
        }

        return $next($request);
    }
}
