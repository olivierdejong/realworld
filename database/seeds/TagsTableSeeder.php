<?php

use App\Models\Article;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Article::all()->each(
            function ($article) {
                for ($i = 0; $i < rand(0, 10); $i++){
                    $article->tags()->save(factory(Tag::class)->make());
                }
            }
        );
    }
}
