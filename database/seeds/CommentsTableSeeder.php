<?php

use App\Models\Article;
use App\Models\Comment;
use App\Models\Profile;
use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $custom_comments = [
            [
                'body' => 'Heel goed artikel, wow!',
            ],
            [
                'body' => 'Waardeloos',
            ],
            [
                'body' => 'Steengoed artikel!',
            ],
            [
                'body' => 'Heel erg goed geschreven',
            ],
            [
                'body' => 'Mooi geschreven!',
            ],
        ];

        $profile_amount = Profile::count();
        $article_amount = Article::count();
        forEach($custom_comments as $attributes){
            $comment = new Comment($attributes);
            $randomProfile = Profile::find(rand(1, $profile_amount));
            $randomArticle = Article::find(rand(1, $article_amount));
            $comment->article()->associate($randomArticle);
            $comment->author()->associate($randomProfile);
            $comment->save();
        }

        factory(Comment::class, 95)->make()->each(
            function ($comment) use ($profile_amount, $article_amount) {
                $randomProfile = Profile::find(rand(1, $profile_amount));
                $randomArticle = Article::find(rand(1, $article_amount));
                $comment->article()->associate($randomArticle);
                $comment->author()->associate($randomProfile);
                $comment->save();
            }
        );
    }
}
