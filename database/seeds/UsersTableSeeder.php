<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Profile;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $user = factory(User::class)->create(
            [
                'email' => 'olivier@hotmail.com',
            ]
        );

        factory(Profile::class)->create(
            [
                'username' => 'Olivier',
                'bio' => 'Ik ben Software Developer en speel graag basketbal',
                'user_id' => $user->id,
            ]
        );

        $user = factory(User::class)->create(
            [
                'email' => 'pauline@hotmail.com',
            ]
        );

        factory(Profile::class)->create(
            [
                'username' => 'Pauline',
                'bio' => 'Ik werk bij EY en hou van muziek en kunst',
                'user_id' => $user->id,
            ]
        );

        $user = factory(User::class)->create(
            [
                'email' => 'piet@hotmail.com',
            ]
        );

        factory(Profile::class)->create(
            [
                'username' => 'Piet',
                'bio' => 'Ik ben loodgieter en hou van tennissen',
                'user_id' => $user->id,
            ]
        );

        $user = factory(User::class)->create(
            [
                'email' => 'tim@hotmail.com',
            ]
        );

        factory(Profile::class)->create(
            [
                'username' => 'Tim',
                'bio' => 'Ik ben Timmerman en hou van voetbal',
                'user_id' => $user->id,
            ]
        );

        factory(User::class, 16)->create()->each(
            function ($user) {
                $user->profile()->save(factory(App\Models\Profile::class)->make());
            }
        );
    }
}
