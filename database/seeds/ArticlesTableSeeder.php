<?php

use App\Models\Article;
use App\Models\Profile;
use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $custom_articles = [
            [
                'title' => 'Technologie',
                'description' => 'dit gaat over technologie',
                'body' => 'blablablbalbalabla dit is iets vlabafjnehfae nog meer troep!',
            ],
            [
                'title' => 'Gezondheid',
                'description' => 'dit gaat over Gezondheid',
                'body' => 'blablablbalbalabla dit is iets vlabafjnehfae nog meer troep!',
            ],
            [
                'title' => 'Vegetarisch',
                'description' => 'dit gaat over Vegetarisch',
                'body' => 'blablablbalbalabla dit is iets vlabafjnehfae nog meer troep!',
            ],
            [
                'title' => 'Wetenschap',
                'description' => 'dit gaat over Wetenschap',
                'body' => 'blablablbalbalabla dit is iets vlabafjnehfae nog meer troep!',
            ],
            [
                'title' => 'Biologie',
                'description' => 'dit gaat over Biologie',
                'body' => 'blablablbalbalabla dit is iets vlabafjnehfae nog meer troep!',
            ],
        ];

        $profiles = Profile::all();
        foreach($custom_articles as $i => $custom_article){
            $article = new Article($custom_article);
            $profiles[$i]->articles()->save($article);
        }

        $profiles->each(
            function ($profile) {
                for ($i = 0; $i < rand(0, 5); $i++){
                    $profile->articles()->save(factory(Article::class)->make());
                }
            }
        );
    }
}
