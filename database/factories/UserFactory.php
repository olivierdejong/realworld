<?php

/**
 * @var Factory $factory 
 */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(
    User::class, function (Faker $faker) {
        return [
        'password' => 'wachtwoord',
        'email' => $faker->unique()->safeEmail,
        ];
    }
);
