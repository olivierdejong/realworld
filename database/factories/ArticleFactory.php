<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Article;
use App\Models\Profile;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(
    Article::class, function (Faker $faker) {

        $title = $faker->word();

        return [
            'title' => $faker->word(),
            'slug' => Str::slug($title),
            'description' => $faker->sentence(),
            'body' => $faker->paragraph(),
        ];
    }
);
