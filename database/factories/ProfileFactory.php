<?php

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */

use App\Models\Profile;
use Faker\Generator as Faker;

$factory->define(
    Profile::class, function (Faker $faker) {
        return [
        'username' => $faker->name,
        'bio' => $faker->sentence(),
        'image' => null,
        ];
    }
);
