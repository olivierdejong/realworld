<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(
            'comments', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->mediumText('body');
                $table->unsignedBigInteger('profile_id');
                $table->unsignedBigInteger('article_id');
                $table->timestamps();
                $table->softDeletes();
            }
        );

        Schema::table(
            'comments', function (Blueprint $table) {
                $table->foreign('profile_id')
                    ->references('id')
                    ->on('profiles');
                $table->foreign('article_id')
                    ->references('id')
                    ->on('articles')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('comments');
    }
}
