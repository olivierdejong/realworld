<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(
            'articles', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('title');
                $table->string('slug');
                $table->text('description');
                $table->longText('body');
                $table->unsignedBigInteger('profile_id');
                $table->timestamps();
                $table->softDeletes();
            }
        );

        Schema::table(
            'articles', function (Blueprint $table): void {
                $table->foreign('profile_id')
                    ->references('id')
                    ->on('profiles');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
