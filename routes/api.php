<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
], function () {
    Route::post('/users/login', 'Api\Auth\LoginController@login');
    Route::get('/users/logout', 'Api\Auth\LoginController@logout')->middleware('jwt');
    Route::post('/users', 'Api\Auth\RegisterController@store');
    Route::get('/user', 'Api\Auth\AuthController@me')->middleware('jwt');
    Route::put('/user', 'Api\Auth\AuthController@update')->middleware('jwt');
    Route::get('/tags/count', 'Api\TagController@count');
    Route::get('/tags', 'Api\TagController@get');

    Route::group([
        'prefix' => 'profiles/{profile}',
    ], function () {
        Route::get('/', 'Api\ProfileController@get');
        Route::post('/follow', 'Api\ProfileController@follow')->middleware('jwt');
        Route::delete('/follow', 'Api\ProfileController@unfollow')->middleware('jwt');
    });

    Route::group([
        'prefix' => 'articles',
    ], function () {
        Route::post('/', 'Api\ArticleController@create')->middleware('jwt');
        Route::get('/feed/', 'Api\ArticleController@feed')->middleware('jwt');
        Route::get('/', 'Api\ArticleController@list');

        Route::group([
            'prefix' => '{article}',
        ], function () {
            Route::get('/', 'Api\ArticleController@get');
            Route::put('/', 'Api\ArticleController@update')->middleware('jwt', 'author');
            Route::delete('/', 'Api\ArticleController@delete')->middleware('jwt', 'author');

            Route::group([
                'middleware' => 'jwt',
                'prefix' => 'favorite',
            ], function () {
                Route::delete('/', 'Api\ArticleController@unfavorite');
                Route::post('/', 'Api\ArticleController@favorite');
            });

            Route::group([
                'prefix' => 'comments',
            ], function () {
                Route::get('/', 'Api\CommentController@get');
                Route::post('/', 'Api\CommentController@create');
                Route::delete('{comment}', 'Api\CommentController@delete')->middleware('author');
            });
        });
    });
});
